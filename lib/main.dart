import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() => runApp(MaterialApp(
  home: HalqaPage(),
  debugShowCheckedModeBanner: false,
  theme: ThemeData(
    primaryIconTheme: IconThemeData(
      color: Colors.teal[700]
    ),
  ),
));



class HalqaPage extends StatefulWidget {
  @override
  _HalqaPageState createState() => _HalqaPageState();
}

class _HalqaPageState extends State<HalqaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: Container(
        width: 160.0,
        child: Drawer(),
      ),
      body: CustomScrollView(
        slivers: <Widget>[

          //Top App Bar Section
          SliverAppBar(
            pinned: false,
            floating: false,
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            expandedHeight: 100.0,
            actions: <Widget>[],
            flexibleSpace: Wrap(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 40.0, top: 24.0),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[

                        //Dates Column
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(height: 7),

                            //Day
                            Container(
                              alignment: Alignment.center,
                              height: 24.0,
                              width: 60.0,
                              decoration: BoxDecoration(
                                color: Colors.teal[700]
                              ),
                              child: Text("الأربعاء",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w600),),
                            ),
                            //Date 1
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text("١٤٤٠/۰٦/۱٦", style: TextStyle(fontSize: 12.0)),
                            ),
                            //Date 2
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text("۲۰۱۹/۰۲/۲۰", style: TextStyle(fontSize: 12.0)),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text("يوم عمل", style: TextStyle(fontSize: 12,color: Colors.teal[700]),),
                            )
                          ],
                        ),
                        SizedBox(width: (MediaQuery.of(context).size.width/2) - 162),

                        //Line
                        Container(
                          width: 0.5,
                          height: 100,
                          decoration: BoxDecoration(
                              color: Colors.black38
                          ),
                        ),

                        SizedBox(width: 8),

                        //Home Page Column (Home Icon With Word)
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(FontAwesomeIcons.home, color: Colors.teal[700],size: 35.0),
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text("الرئيسية",
                                  style: TextStyle(fontSize: 13.0,fontWeight: FontWeight.w600)),
                            )
                          ],
                        ),
                        SizedBox(width: 12.0),

                        //Line
                        Container(
                          width: 0.5,
                          height: 100,
                          decoration: BoxDecoration(
                            color: Colors.black38
                          ),
                        ),
                        SizedBox(width: 43.0),

                        //Right Column
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                SizedBox(width: 3.0),
                                Text("إختياريه",style: TextStyle(color: Colors.black,fontSize: 16.0)),
                                Text(" :مقرأة",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w600)),
                              ],
                            ),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                SizedBox(width: 15),
                                Text("تجريبية",style: TextStyle(color: Colors.black,fontSize: 15.0)),
                                Text(" :حلقة",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16.0,fontWeight: FontWeight.w600)),
                              ],
                            ),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text("شرق جدة",style: TextStyle(color: Colors.black,fontSize: 15.0)),
                                Text(" :إشراف",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16.0,fontWeight: FontWeight.w600)),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),

          //Student List
          SliverList(
            delegate: SliverChildBuilderDelegate((_, int index){
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 3.0, right: 6.0),
                    child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[

                        //Revision Section
                        Container(
                          alignment: Alignment.center,
                          height: 50.0,
                          width: 50.0,
                          decoration: BoxDecoration(
                            color: Colors.teal[700],
                            shape: BoxShape.circle,
                          ),
                          child: Text("مراجعات الفيل ۱",style: TextStyle(
                              color: Colors.white,
                              fontSize: 10
                          ),textAlign: TextAlign.center,),
                        ),

                        SizedBox(width: 9.0),

                        //Not Found Word
                        Container(
                          alignment: Alignment.center,
                          height: 50.0,
                          width: 50.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: 3,
                              color: Colors.teal[700]
                            ),
                          ),
                          child: Text("لا يوجد",style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 13
                          ),),
                        ),

                        SizedBox(width: 8.0),

                        //Send Icon
                        Transform.rotate(
                          angle: 5.80,
                          child: IconButton(
                            color: Colors.teal[700],
                            icon: Icon(Icons.send),
                            onPressed: () {},
                          ),
                        ),

                        SizedBox(width: 9.0),

                        //Right Column With Name
                        Column(
                          children: <Widget>[
                            SizedBox(height: 10),
                            Text("عبدالله عدنان جمل",style: TextStyle(fontWeight: FontWeight.bold)),
                            Padding(
                              padding: const EdgeInsets.only(left: 23),
                              child: Text("يحفظ: ٠ جزء",style: TextStyle(
                                color: Colors.grey[600],
                                fontSize: 13
                              )),
                            ),
                            Container(
                              padding: const EdgeInsets.only(left: 30),
                              child: Text(":الاختبار القادم",style: TextStyle(
                                color: Colors.grey[600],
                                fontSize: 11.0
                              ),),
                            )
                          ],
                        ),

                        //Person Icon
                        CircleAvatar(
                          maxRadius: 23,
                          child: Icon(Icons.person,color: Colors.white, size: 30,),
                          backgroundColor: Colors.teal[700],
                        ),
                      ],
                    ),
                  ),
                ],
              );
            })
          )
        ],
      ),
    );
  }
}